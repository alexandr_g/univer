from django.db import models
from django.contrib.auth.models import User


class ChartFilterCriterion(models.Model):
    criterion_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)


class ChartFilterValue(models.Model):
    value_id = models.AutoField(primary_key=True)
    value = models.CharField(max_length=100)
    user = models.ForeignKey(User)
    user = models.ForeignKey(ChartFilterCriterion)


class ClientModule(models.Model):
    module_id = models.AutoField(primary_key=True)
    module_version = models.CharField()
    module_sn = models.CharField()
    user = models.ForeignKey(User)


class ModuleSchema(models.Model):
    schema_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)
    schemaurl = models.CharField(max_length=256)
    module = models.ForeignKey(ClientModule)


class State(models.Model):
    name = models.CharField(max_length=256)
    state_id = models.AutoField(primary_key=True)


class Element(models.Model):
    element_id = models.AutoField(primary_key=True)
    element_sn = models.CharField(max_length=256)
    state = models.ForeignKey(State)


class ElementInfo(models.Model):
    element_info_id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=1024)
    name = models.CharField(max_length=256)
    element = models.ForeignKey(Element)


class IconCatalog(models.Model):
    name = models.CharField(max_length=256)
    catalog_id = models.AutoField(primary_key=True)


class Icon(models.Model):
    icon_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=1024)
    iconurl = models.CharField(max_length=256)
    catalog = models.ForeignKey(IconCatalog)


class Widget(models.Model):
    widget_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)
    svgPath = models.TextField()
    schema = models.ForeignKey(ModuleSchema)
    icon = models.ForeignKey(Icon)
    element = models.ForeignKey(Element)


class UserInfo(models.Model):
    info_id = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=256)
    lastname = models.CharField(max_length=256)
    avatar_url = models.CharField(max_length=256)
    user = models.ForeignKey(User)


class AuthenticationLog(models.Model):
    logentry_id = models.AutoField(primary_key=True)
    date = models.DateTimeField()
    session = models.CharField(max_length=256)
    IP = models.CharField(max_length=128)
    user = models.ForeignKey(User)


class MessageType(models.Model):
    message_type_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=1024)


class Message(models.Model):
    message_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)
    message_code = models.IntegerField()
    message_type = models.ForeignKey(MessageType)
    element = models.ForeignKey(Element)


class MessagesLog(models.Model):
    log_entry_id = models.AutoField(primary_key=True)
    date = models.DateTimeField()
    response_code = models.IntegerField()
    messsage = models.ForeignKey(Message)

class ChartFilterCriterion :
	def __init__(self) :
		self.criterion_id = None # int
		self.name = None # str
		pass
class MessagesLog :
	def __init__(self) :
		self.log_entry_id = None # int
		self.date = None # datetime
		self.response_code = None # int
		pass
class Widget :
	def __init__(self) :
		self.widget_id = None # int
		self.name = None # str
		self.svgPath = None # str
		pass
class AuthenticationLog :
	def __init__(self) :
		self.logentry_id = None # int
		self.date = None # datetime
		self.session = None # str
		self.IP = None # str
		pass
class ClientModule :
	def __init__(self) :
		self.moduleSN = None # uuid
		self.module_version = None # str
		pass
class ChartFilterValue :
	def __init__(self) :
		self.value_id = None # int
		self.value = None # str
		pass
class ModuleSchema :
	def __init__(self) :
		self.schema_id = None # int
		self.name = None # str
		self.schemaurl = None # str
		pass
class MessageType :
	def __init__(self) :
		self.message_type_id = None # int
		self.name = None # str
		self.description = None # str
		pass
class Element :
	def __init__(self) :
		self.elementID = None # int
		self.elementSN = None # uuid
		pass
class State :
	def __init__(self) :
		self.name = None # str
		self.state_id = None # int
		pass
class User :
	def __init__(self) :
		self.user_id = None # int
		self.username = None # str
		self.password_hash = None # str
		self.last_login_date = None # datetime
		pass
class ElementInfo :
	def __init__(self) :
		self.element_info_id = None # int
		self.description = None # str
		self.name = None # str
		pass
class IconCatalog :
	def __init__(self) :
		self.name = None # str
		self.catalog_id = None # int
		pass
class Message :
	def __init__(self) :
		self.messageID = None # int
		self.name = None # str
		self.message_code = None # long
		pass
class Icon :
	def __init__(self) :
		self.icon_id = None # int
		self.name = None # str
		self.description = None # str
		self.iconurl = None # str
		pass
class UserInfo :
	def __init__(self) :
		self.info_id = None # int
		self.firstname = None # str
		self.lastname = None # str
		self.avatar_url = None # str
		pass

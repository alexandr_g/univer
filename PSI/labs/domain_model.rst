User authentication. 
----------------------

    1. Bussiness transactions.
    2. Articolele care participa la tranzactie.
    3. Product or service related to transaction or to transaction items.
    4. Where is it transaction recorded.
    5. Roles of peoples/organisations from UseCase (actors).
        *User*
    6. Place of transactions.
    7. Physical objects
    8. Description of things.
    9. Catalogs (catalog de produse, prezentare a ceva, ceva ce contine
       informatiii)
       *UserInfo*
    10. Container of things.
    11. Things in container.
    12. Other systems (sistemele cu care colaboreaza alte sisteme externe).
    13. Records of finance.
        *AuthenticationSession*
    14. Transaction instruments.
    15. Schedules, manuals, documents reffered to use case.

Visualization of the general state of the system
----------------------

    1. Bussiness transactions.
        *SystemSchema*
    2. Articolele care participa la tranzactie.
        *Widget*
    3. Product or service related to transaction or to transaction items.
        *Icon*
    4. Where is it transaction recorded.
    5. Roles of peoples/organisations from UseCase (actors).
        *User*
    6. Place of transactions.
    7. Physical objects
        *Element*
    8. Description of things.
        *ElementInfo*
    9. Catalogs (catalog de produse, prezentare a ceva, ceva ce contine
       informatiii)
       *IconsCatalog*
    10. Container of things.
    11. Things in container.
    12. Other systems (sistemele cu care colaboreaza alte sisteme externe).
        *ClientModule*
    13. Records of finance.
    14. Transaction instruments.
    15. Schedules, manuals, documents reffered to use case.

Visualization of charts
----------------------

    1. Bussiness transactions.
        *Element*
    2. Articolele care participa la tranzactie.
        *ChartFilterValue*
    3. Product or service related to transaction or to transaction items.
        *ChartFilterCriterion*
    4. Where is it transaction recorded.
    5. Roles of peoples/organisations from UseCase (actors).
        *User*
    6. Place of transactions.
    7. Physical objects
    8. Description of things.
    9. Catalogs (catalog de produse, prezentare a ceva, ceva ce contine
       informatiii)
    10. Container of things.
    11. Things in container.
    12. Other systems (sistemele cu care colaboreaza alte sisteme externe).
        *ClientModule*
    13. Records of finance.
    14. Transaction instruments.
    15. Schedules, manuals, documents reffered to use case.

Changing the status of the active element
----------------------

    1. Bussiness transactions.
        *Command*
    2. Articolele care participa la tranzactie.
        *Element*, *State*
    3. Product or service related to transaction or to transaction items.
        *CommandType*
    4. Where is it transaction recorded.
        *CommandsLog*
    5. Roles of peoples/organisations from UseCase (actors).
        *User*
    6. Place of transactions.
    7. Physical objects
        *Element*
    8. Description of things.
        *ElementInfo*
    9. Catalogs (catalog de produse, prezentare a ceva, ceva ce contine
       informatiii)
    10. Container of things.
    11. Things in container.
    12. Other systems (sistemele cu care colaboreaza alte sisteme externe).
        *ClientModule*
    13. Records of finance.
    14. Transaction instruments.
    15. Schedules, manuals, documents reffered to use case.

Visualization of user's dashboard
----------------------

    1. Bussiness transactions.
    2. Articolele care participa la tranzactie.
        *Element*, *ClientModule*, *Message*, *MessageType*
    3. Product or service related to transaction or to transaction items.
    4. Where is it transaction recorded.
    5. Roles of peoples/organisations from UseCase (actors).
        *User*
    6. Place of transactions.
    7. Physical objects
    8. Description of things.
    9. Catalogs (catalog de produse, prezentare a ceva, ceva ce contine
       informatiii)
    10. Container of things.
    11. Things in container.
    12. Other systems (sistemele cu care colaboreaza alte sisteme externe).
    13. Records of finance.
    14. Transaction instruments.
    15. Schedules, manuals, documents reffered to use case.

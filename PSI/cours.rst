PSI
===

============
05-Sep-2014
============

Software development methods
    **Agile** este utilizata pentru interactiunea mai intensiva cu
    clientul, se pot modifica des cerintele, testarea se face concomitent
    cu elaborarea, se aloca mai putina atentie la testare, este strins
    legata de aceea cum decurg startup-urile.
    **Waterfall** este utilizata la proiecte mai mari, de lunga durata, se
    refera la proiecte critice, etapele se executa secvential, dupa
    fiecare etapa urmeaza atit testarea codului, cit si reviewul
    specificatiilor etc. Dezavantajul este durata mai lunga de elaborare.

Sistem critic
    un sistem este critic daca erorile/bug-urile acestui sistem pod duce
    la pierderi semnificative financiare, umane etc.

Etapele de creare a unui produs:
--------------------------------
    - Analiza
    - Modelarea
    - Proiectarea
    - Implementarea
    - Testarea
    - Integrarea
    - Deployment
    - Project management
    - Training (optional)

Rational unified process:
-------------------------
    1. **Inception** - etapa la care se discuta daca proiectul poate fi
       realizat. Se fac citeva use-case-uri, se face design-ul proiectului
       (componente, clase).
    2. **Elaboration** - etapa in care se orienteaza la analiza
       cerintelor, care nu s-au analizat in etapa precedenta. Se incepe
       implementarea a partilor cele mai importante din sistem.
       Se orienteaza la fel spre modelarea sistemului.
    3. **Construction** - se orienteaza la implementarea sistemului. Poate
       se proiecteaza careva parti din sistem care ramin din etapa
       urmatoare. Se incepe testarea. Integrarea se contine in etapa data
       - se integreaza modulele separate etc.
    4. **Transition** - se efectuiaza trecerea produsului de la executori
       la clienti. Se efectuiaza testarea, deployment.

12-Sep-2014
------------

:Cerinte functionale:    functionalitatea care **trebuie** sa puna la dispozitie aplicatia.

Functional Usability Relaybility Performance Supportability


19-Sep-2014
------------

Use Case: Process sale
-----------------------

:Scope: 
    POS application
:Level:
    User goal
:Primary autor:
    Cashier
:Stakeholders & Interests:
    * Cashier - 
    * Customer - 
    * Store manager - 
    * Payment Authorization System (instanta ce elibereaza cardurile) - 
    * Governement Tax Agency (taxele) - 
    * Supplier - 

**Preconditions**
    1. Casierul trebuie sa fie autorizat in sistem (authenticated &
       authorized).

**Postconditions**
    0. Sale was saved.
    1. Receipt (bonul de plata).
    #. Inventory & Accounting were updated.
    #. Payment authorization approval was recorded.

**Main scenario**
    1. Clientul vin cu marfa.
    2. Casierul initializeaza o vinzare. (Cashier creates new sale).
    3. Cashier enters item ID.
    4. System displays item description (name, price, total price).
        ''Pasul 3 si 4 se repete pina sunt produse de introdus.''
    5. System displays total with taxes.
    6. Cashier tells Customer the total & ask for payment.
    7. Customer pays & system handles the payment.
    8. System complete the sale and updates accounting and inventory.
    9. System prints the receipt.
    10. Cashier gives the receipt to Customer.

**Alternative scenaries**
    * **a** System fails
        1. Sistemul se porneste.
        2. Casierul se autorizeaza.
        3. Vinzarea care era in curs de procesare se recupereaza.
        4. Casierul continua de la pasul la care s-a oprit.
    * **b** Stop sale processing
        1. Vinzatorul apeleaza la manager.
        2. Managerul se autorizeaza.
        3. Managerul anuleaza vinzarea curenta.
        4. Sistemul sterge vinzarea curenta si toate articolele din
           vinzare.
**2a** (scenariul alternativ la pasul 2 din scenariul de baza)
    Cashier opens a suspended sale.
        1. Cashier resume the sale by ID.
        2. System afiseaza info despre sale si total price.
            a. Sale with given ID didn't found.
                1. System displays error message.
                2. Cashier starts new sale.
        3. Cashier continue to enter items ID. 
            a. No such ID.
                1. System prints the error message (ScanError).
                2. Cashier enters ID manually.
                3. 
                    a. No such item in system.
                        1. Manager enter item in system.
                        1. Customer refuse the item.
            b. There are more items of same type.
                1. Cashier enters iems quantity.
            c. Same ID for more items.
                1. Casierul apeleaza la manager.
                2. Managerul se autentifica si corecteaza produsul
                   (produsele) in plus.
                3. Casierul reintroduce ID-ul.
        4.
            a. Produsul nu corespunde cu ID-ul sau (pretul si descrierea).
                1. Casierul apeleaza la manager.
                2. Managerul se autentifica si sterge produsul din lista
                   vinzarii.
                3. Casierul continua.
        5. 
        6. 
        7. 
            a. Cash.
                1. Clientul ofera suma de bani.
                2. Casierul introduce suma in sistem.
                3. Sistemul calculeaza restul.
            b. Card bancar.
        8. 
        9. (se termina hirtia, cerneala, se opreste sistemul)
        10. 

Cerintele speciale.
-------------------

Cerinte speciale pentru acest UseCase.
    * Diferite scanere pentru barcode.
    * Scanere pentru cod.
    * Timpul normal de asteptare pentru autorizarea achitarii sa fie 3
      sec, timpul maxim 30 sec.

Technology and data validation.
-------------------------------

Putem specifica tehnologiile pe care le folosim:
    * Scanere portabile/statice.
    * Monitor pentru pret
    * etc.

Frecventa de utilizare.
--------------------------

Utilizarea permanenta.

Intrebarile deschise.
---------------------

Se intreaba lucrurile care nu sunt clare, bine determinate.
    * Taxele produselor (nu cunoastem care sunt taxele la toate produsele).


26-Sep-2014
------------

Formarea UseCase-urilor:
    * Determinam ce necesitati are user-ul/actorii. Cine sunt actorii
      sestemului.
    * De ce date dispune sistemul si cum apar/se schima aceste date.
    * Se stabilesc limitele sistemului.
    * Se definesc useCase-urile pentru a satisface scopurile actorilor.


Intrebari pentru a determina actorii si cazurile de utilizare /
necesitatile.
    * Cine opreste si porneste sistemul.
    * Ce face managerul?
    * Cine face administrarea sistemului?
    * Cine este actorul care interactioneaza cu sistemul?


Elaboration
-------------

In urma etapei **Inception** avem specificatiile, cerintele, glosar, partial
use-caseurile.

In urma elaborarii obtinem - design-ul, cele mai importante parti din
sistem implementate.
In design avem - data model, domain model, system architecture.

Etapa data (elaborare) are mai multe etape. Each stage should be documented - riscs,
priority. This is relative to UseCases.

*Ce face arhitectu in aceasta etapa?*
El determina clasele de baza din use case. Se face diagrama domain model.
Domain model - sunt doar clasele selectate din UseCase-uri. Aceste sunt
clase de domeniu (bussiness classes).
Domain model este o diagrama de clasa, in care figureaza doar relatii de
asociere. Atributele principale cele mai principale la fel se indica in
domain model (ca de exemplu ora pentru vinzare, ID-ul pentru produs).

**Dupa use-caseul precedent**
Metode de identificare a claselor din domeniu:

1. Bussiness transactions.
   Sale, Payment
2. Articolele care participa la tranzactie.
   SaleItem
3. Product or service related to transaction or to transaction items.
   Item
4. Where is it transaction recorded.
   Registru
5. Roles of peoples/organisations from UseCase (actors).
   Cashier, Client, Store manager
6. Place of transactions.
   Store
7. Physical objects
   Item
8. Description of things.
   ItemDescription
9. Catalogs (catalog de produse, prezentare a ceva, ceva ce contine
   informatiii)
   ProductCatalog
10. Container of things.
    Store
11. Things in container.
    Item
12. Other systems (sistemele cu care colaboreaza alte sisteme externe).
    TaxInterface, PaymentInterface
13. Records of finance.
    Recip (cec), Leger
14. Transaction instruments.
    Cash, BankAccount, Cheque
15. Schedules, manuals, documents reffered to use case.

Classes
    * Sale [ID, date_time]
    * Item [item_id]
    * SaleLineItem [quantity]
    * Payment [amount]
    * ItemDescription [price, description, ID]
    * ProductCatalog 
    * Store [address, name]
    * Leager 
    * Receipt
    * Cashier [ID]
    * Customer
    * TaxInterface
    * PaymentAuthorizationSystem
    * BankAccount
    * Cash
    * Register [ID]

Sale [1] <--(is for)-- [1] Customer
# Sale ---- Cashier # este print relatia Sale --- Register --- Cashier
Sale [1] --(contained in)-- [1..*] SaleLineItem [0..1] --(record sale of)--> [1..*] Item [1] <--(describes)-- [1] ItemDescription 
ItemDescriptions [1..*] --(contains)-- [1] ProductCatalog [1] --(used by)-- [1] Store
Item [1..*] --(stocks)-- [1] Store
Payment [1] <--(paid by)-- [1] Sale
Sale [1] --(confirmed by)-- [1..*] Receipt
Store [1] --(houses)-- [1..*] Register [1] --(captured in)--> [0..*] Sale
Cashier [1] --(works on)--> [1] Register
Store [1] --(records account for)-- [1] Leager [1] --(logs completed)--> [1..*] Sale


10-Oct-2014
-------------

System Sequence Diagrams
=========================

SSD analizeaza sistemul ca un black box si reprezinta interactiunea intre
sistem si actori.

Actorul principal: **Cashier**
Obiect: **POS**

Cashier                       POS
    |
    | ---[ newSale() ]-------> |
    |                          | # aceasta zona intr-un dreptunghi cu loop 
    | ---[ enterItem(...) ] -> | # (cuvintul loop in top-left corner.
    | -[ show descr, total ]-- | #
    |                          |
    | -----[ endSale() ]---->  |
    | ----[ total with taxes]- |
    |                          |
    | ----[ makePayment() ]--  |
    | ----[ receipt       ]-   |

# lost of data. Bl#a

17-Oct-2014
-------------


24-Oct-2014
-------------


Design patterns.
------------------

Pattern-urile se utilizeaza pentru a distribui corect responsabilitatile
intre clase.

Patternurile G.R.A.S.P (General Responsabilites Assignement Software
Patterns).
Sunt 9 pattern-uri care intra in aceasta categorie.

Alte pattern-uri sunt G.o.F. (Gang of Four).

GRASP sunt mai generale decit GOF.
Pentru a incepe distribui responsabilitatile intre clase trebuie sa avem
- clase (obtinute din domain model), cerinte (din use-case-uri) si cerinte
  (din contracte) - dupa esenta scenariul.
- examinam use-caseurile, contractele. In baza la contracte continuam
  elaborarea design-ului, adaugind metode, clase noi, distribuind
  responsabilitatile. Se utilizeaza majoritatea pattern-urilor.

In urma acestor pasi obtinem mai multe diagrame de clasa, interactiune
(sequence, communication), care oricum se bazeaza pe domain model.

Responsabilitatile claselor
    - Realizeaza ceva, face ceva.
    - Pastreaza careva date, cunoaste informatii.
    - Implementeaza o parte din scenariu.
    - Sa gestioneze resursele (obiecte, activitatile etc.)

:Pattern:
    se descrie prin nume, problema pe care o rezolva si solutia.

**Pattern-urile GRASP**
    - Creator
    - Information expert
    - Low coupling
    - Controller
    - High Cohesion
    - Polymorphism
    - Pure fabrication
    - Indirection
    - Protected variations

Creator
-------

Problema
    cine este responsabil de crearea instantei unei clase
Solutia
    atribuirea responsabilitatii clasei B de a crea instannta unei clase
    A, atunci cind
        a. B contine A (relatia de agregare sau compozitie)
        b. B inregistreaza A
        c. B are date de instantiere pentru A
        d. B strins utilizeaza A
    Deci, B este creator pentru A.

In cazul POS-ului, responsabil de crearea instantelor de SaleLineItem este
clasa Sale.

Patternurile cu principii asemanatoare - Low coupling (GRASP), concrete
factory, abstract factory (GoF).

Information expert (expert)
----------------------------

Problema
    care este principiul general de atribuire responsabilitatilor unui
    obiect.
Solutia
    de atribuit responsabilitatea unei clase expert, ce detine informatia
    necesara pentru a realiza responsabilitatea.
Exemplu
    ItemDescription -> Item -> SaleLineItem -> Sale -> Payment
Pattern-urile asemanatoare sunt - low coupling, high cohesion.

Controller
------------

Problema
    care este primul obiect dupa UI care primeste si coordoneaza
    operatiile de sistem.
Solutia
    de atribuit responsabilitatea unei clase care prezinta una din
    urmatoarele optiuni:
        1. Reprezinta in general sistemul sau este un obiect root
        #. Este in subsystem major sau este un device pe care ruleaza
           soft-ul.
        #. Reprezinta un scenariul a use-caseului in cadrul caruia se
           produce event-ul (Se numeste de obicei \*Handler,
           session, controller.).
    In caz-ul POS-ului ca controller poate fi clasa Register, daca nu s-ar
    exista, atunci s-ar crea o clasa ProcessSaleCoordinator (UseCase title
    + Coordinator/Controller/Handler).
Acest pattern este in legatura cu - Pure Fabrication (GRASP), Fasad,
Layers, Command (GoF).

Low coupling
-------------

Problema
    cum de suportat dependenta joasa, impact mic si de a mari utilizarea.
Solutie
    atribuirea responsabilitatii in asa fel ca sa fie cuplare joasa si
    pentru realizarea corecta trebuie sa se faca comparatie - sa se
    evaluieze alternative pentru aceste operatii.

Dezavantajele cuplarii inalte
    - extensibilitatea dificila,
    - prea multe responsabilitati

In cazul POST-ului Sale -> Payment, luind in consideratie si pattern-ul
Information Expert.

High cohesion
--------------

Cohesion - proprietatea de a tine responsabilitatile orientate spre un
anumit scop. Cohesiunea inalta - responsabilitatile sunt orientate strict
spre scop. Cohesiunea joasa - invers, duce la marirea de legaturi etc.
Problema
    cum de mentinut obiectele focusate, usor de gestionat si sa suporte
    pattern-ul Low coupling.
Solutia
    de a distribui responsabilitatile in asa fel ca sa se pastreze
    cohesiunea inalta. Se obtine prin compararea alternativelor.

Problemele nerespactarii
    - mai greu se mentite,
    - scade reutilizabilitatea,
    - etc.

Facem design pe domain model.
    - diagrame de clase
   

Facem pentru contractul *makePayment()*

Apar operatii noi:
Register
    + newSale()

Sale
    + Sale()

+----------+---------------------+
| Pattern  | Classes             |
+----------+---------------------+
| Creator  | Register            |
|          +---------------------+
|          | Sale                |
|          +---------------------+
|          | Sale.create         |
|          +---------------------+
|          | List<...>.create    |
+----------+---------------------+
| Contr.   | Register            |
+----------+---------------------+

Pe diagrama aceste pattern-uri ca notite - dreptunghiuri cu text si
legaturi cu linie solida la obiectele cu care sunt legate. 
        +---------------+
        | :Register     |
        +---------------+
            |
            |                         +-------+
            | ------------create----> | :Sale |
            |                         +-------+
            |                           |                      +--------------------+
            |                           | ----------create---> | List<SaleLineItem> |
            |                           |                           |
            |                           |                           |



21-Nov-2014
------------

GoF
------

Adapter
=========

Problema
    Cum de rezolvat interfete incompatibile sau de a pune o interfata
    stabila componentelor similare cu diferite interfete

Solutia
    se aduce o interfata noua/intermediar si se convertesc toate
    interfetele necesare in una, care este adapter.


Lucrarea de curs
------------------

1. Descrierea proiectului
2. Cum ar fi prezentata de client.
3. Descrierea specificatiilor si a cerintelor
4. Model Use-Case (c.v.d. detaliat Post, Pre, Scenariul, Scenariul
   alternativ)
   Diagrama UseCase
5. Alte cerinte (nefunctionale)
6. SSD
7. Domain model (pentru fiecare UseCase)
8. 
9. Utilizarea sabloanelor (in diagrame clase/interactiune sa se puna
   notite cu sabloane utilizate)
10. Diagramele de stare si activitate (diagramele de activitate pentru
    procesele mai complexe, diagramele de stare pentru obiectele claselor
    DOAR [fara GUI] pentru clasele mai complexe cu mai multe stari)
11. Arhitectura logica
12. Diagrama de componente si deployment
13. Implementarea in baza arhitecturii.

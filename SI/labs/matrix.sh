#!/bin/bash

declare matrix_order
declare -A init_matrix
declare -A new_matrix
declare start_row
declare start_col
echo "Provide matrix order:"
read matrix_order


function showMatrix() {
		echo ""
		echo "Your matrix:"
		local n=$matrix_order
		for (( i = 0; i < $matrix_order; i++ )); do
			for (( j = 0; j < $matrix_order; j++ )); do
				printf "%3s" ${init_matrix[$i, $j]}
			done
			echo ""
		done
}

function generateNewMatrix() {
	local new_order=$(($matrix_order * 2 + 1))
	start_row=1
	start_col=$matrix_order
	local tmp
	echo ""
	echo "New matrix:"
	for (( i = 0; i < $new_order; i++ )); do
		for (( j = 0; j < $new_order; j++ )); do
			new_matrix[$i, $j]=0
		done
	done
	for (( i = 0; i < $matrix_order; i++ )); do
		for (( j = 0; j < $matrix_order; j++ )); do
			new_matrix[$start_row, $start_col]=${init_matrix[$i, $j]}
			start_row=$(($start_row + 1))
			start_col=$(($start_col + 1))
		done
		start_row=$(($i + 2))
		start_col=$(($matrix_order - $i - 1))
	done
	for (( i = 0; i < $new_order; i++ )); do
		for (( j = 0; j < $new_order; j++ )); do
			printf "%3s" ${new_matrix[$i, $j]}
		done
		echo ""
	done
}

echo "You have to add $(( $matrix_order * $matrix_order )) elements:"
for (( i = 0; i < $matrix_order; i++ )); do
	for (( j = 0; j < $matrix_order; j++ )); do
		read user_input
		init_matrix[$i, $j]=$user_input
	done
done

showMatrix
generateNewMatrix

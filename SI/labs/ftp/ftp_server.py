#!/usr/bin/python
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
from pyftpdlib import log
import logging


DEFAULT_ADDRESS = '127.0.0.1'
DEFAULT_PORT = 14141


def main():
    log.LEVEL = logging.INFO
    authorizer = DummyAuthorizer()
    authorizer.add_user('user', '12345', 'shared', perm='elradfmwM')
    handler = FTPHandler
    handler.authorizer = authorizer
    handler.permit_foreign_addresses = True
    handler.passive_ports = range(60000, 61000)
    handler.banner = "pyftpdlib based ftpd ready."
    address = (DEFAULT_ADDRESS, DEFAULT_PORT)
    server = FTPServer(address, handler)
    server.serve_forever()

if __name__ == '__main__':
    main()

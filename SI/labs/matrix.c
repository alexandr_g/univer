#include<stdio.h>
#include<malloc.h>
 
void showMatrix(int **arr2d,int n)
{
    int i,j;
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
            printf("%-3d ",arr2d[i][j]);
        printf("\n");
    }
}

void showInnerMatrix(int **arr2d,int n)
{
    printf("%-3d ",arr2d[1][3]);
    printf("\r\n");
    printf("%-3d ",arr2d[2][2]);
    printf("%-3d ",arr2d[2][4]);
    printf("\r\n");
    printf("%-3d ",arr2d[3][1]);
    printf("%-3d ",arr2d[3][3]);
    printf("%-3d ",arr2d[3][5]);
    printf("\r\n");
    printf("%-3d ",arr2d[4][2]);
    printf("%-3d ",arr2d[4][4]);
    printf("\r\n");
    printf("%-3d ",arr2d[5][3]);
    printf("\r\n");
}
 
void createMatrix(int ***twoDarr,int *order)
{
    int i = 0;
    int ord = 7;
    int **matrix;
    matrix = malloc(ord * sizeof(int *));
    for(i=0;i<ord;i++)
    {
        matrix[i] = malloc(ord * sizeof(int *));
    }
    *twoDarr = matrix;
    *order = ord;
}

void fillMatrix(int **matrix)
{
    int i, j;
    int order = 7;
    int **array = matrix;
    for(i=0;i<order;i++)
    {
        for(j=0;j<order;j++)
        {
            array[i][j] = 0;
        }
    }
    array[0][0] = 5;
    array[0][order - 1] = 5;
    array[order - 1][0] = 5;
    array[order - 1][order - 1] = 5;
    array[3][3] = 5;
    array[0][2] = 1;
    array[0][4] = 1;
    array[1][3] = 1;
    array[1][5] = 2;
    array[1][1] = 4;
    array[2][2] = 4;
    array[2][0] = 7;
    array[2][4] = 2;
    array[2][6] = 3;
    array[3][1] = 7;
    array[3][3] = 5;
    array[3][5] = 3;
    array[4][0] = 7;
    array[4][2] = 8;
    array[4][4] = 6;
    array[4][6] = 3;
    array[5][1] = 8;
    array[5][3] = 9;
    array[5][5] = 6;
    array[6][2] = 9;
    array[6][4] = 9;
}

int main()
{
    int **twoDarr,n;
 
    createMatrix(&twoDarr,&n);
    fillMatrix(twoDarr);
    showMatrix(twoDarr, n);
    printf("\r\n Inner matrix: \r\n");
    showInnerMatrix(twoDarr, n);
    return 0;
}

========================
SI - Sec. Inf.
========================


8-Sep-2014
-----------

**Moyenne de comunication**:
    1. L'air
    2. Cables
            - fibre optique 1Gbps/8-10Gbps
            - coaxial 10Mbps
            - torsade (Ethernet) - 1Gbps [120M],

Dispozitive
------------
    1. HUB
           este un devie cu mai multe porturi. Daca un client transmite
           date unui destinatar, HUB-ul va transmite datele tuturor
           clientilor cu exceptia emitorului.
    2. Switch
           **MAC adresa** - este adresa cartelei de retea. Adresa este compusa
           din 2 parti: codul producatorului si codul cartelei.
           Pe baza legaturii MAC <-> user-ul de la port-ul switch-ului,
           pachetele se transmit doar la destinatar.
    3. Repetitor
           este un dispozitiv care multiplica semnalul.

Tipuri de retele
------------------
    1. Anneaux (inel)
           simpla topologie inel.
    2. Bus (magistrala)
           esita 2 tipuri de conexiune. In forma literei T si conexiunea
           care inchide magistrala. T-ul permite conectarea clientilor.
           Viteza este ~10Mbps
    3. Etoile (stea)
    4. Mesh

Clase de IPv4
--------------

In IPv4 avem 4 grupuri a cite 8 bits.

Clasele:
* Clasa A: 1.0.0.0 - 126.255.255.255
  Maska retelei este /8 (255.0.0.0)

to take from classmates
-----------------------




23-Sep-2014
-----------

Impartirea subretelelor:
------------------------

Initial IP: **10.10.10.0/23**

1. 10.10.10.0
2. mask: 255.255.254.0
3. 2 ^ (32 - 23) = 512
4. nr PC = 512 - 2 = 510
5. adr. broadcast: 10.10.11.255
6. FIP = 10.10.10.1
7. LIP = 10.10.10.254

    **1 subretea**

    10.10.10.0/26

    1. 10.10.10.0
    #. 255.255.255.128
    #. 2^7 = 128
    #. nrPC = 126
    #. adr.broad = 10.10.10.127
    #. FIP = 10.10.10.1
    #. LIP = 10.10.10.126

    **2 subretea**

    10.10.10.0/25

    1. 10.10.10.128
    2. 255.255.255.128
    #. 2 ^ 7 = 128
    #. 126
    #. broad = 10.10.10.255
    #. FIP = 10.10.10.129
    #. LIP = 10.10.10.254

    **3 subretea**

    10.10.11.0/24

    1. 10.10.11.0
    #. 255.255.255.0
    #. 2 ^ (32 - 24) = 2 ^ 8 = 256
    #. 254
    #. broad = 10.10.11.255
    #. FIP = 10.10.11.1
    #. LIP = 10.10.11.254

Initial IP: **172.25.0.0/15 | 5**

Greseala in conditia. In HostID este unitate. Eliminam unitatea.

Changed to **172.24.0.0/15 | 5**

log(b=2, v = 5) <= 3

+--------+-----+
| 000    |     |
+--------+     +
| 001    |     |
+--------+     +
| 010    | I   |
+--------+     +
| 011    |     |
+--------+-----+
| 100    | II  |
+--------+-----+
| 101    | III |
+--------+-----+
| 110    | IV  |
+--------+-----+
| 111    | V   |
+--------+-----+

1. 172.24.0.0/15
#. adr.ret. 172.24.0.0
#. mask = 255.254.0.0
#. nr.host = 2 ^ (32 - 15) = 131 072
#. nrPC = nrHost - 2 = 131 070
#. broad = 172.255.255.255
#. FIP 172.24.0.1
#. LIP 172.25.255.254

    **1 subretea**
    172.24.0.0/16
    
    1. addr. 172.24.0.0
    #. mask. 255.255.0.0
    #. nr.host = 2 ^ 16 = 65536
    #. nr.PC = 65534
    #. broad = 172.24.255.255
    #. FIP = 172.24.0.1
    #. LIP = 172.25.255.254

    **2 subretea**
    172.25.0.0/18
    1. addr. 172.25.0.0
    #. mask  255.255.192.0
    #. nr.host = 2 ^ (32 - 18) = 2 ^ 14 = 16 384
    #. nr.PC = 16 382
    #. broad = 172.25.63.255
    #. FIP = 172.25.0.1
    #. LIP = 172.25.63.2

    **3 subretea**
    172.25.64.0 / 18
    1. addr. 172.25.64.0
    2. mask  255.255.192.0
    #. nr.host = 16 384
    #. nr.PC = 16 382
    #. broad = 172.25.127.255
    #. FIP = 172.25.64.1
    #. LIP = 172.25.127.254

    **4 subretea**
    172.25.128.0 / 18
    1. addr. 172.25.128.0
    2. mask 







07-Oct-2014
------------

Some condition
----------------

---
1
---

Este dat spectrul de IP-uri
    192.168.0.126 - 192.168.0.129

    1. Determinam clasa (**C**) si maska clase (**255.255.255.0**)
    2. Determinam NETID din IP si maska (**192.168.0.0**)

**Acest spectru nu poate fi impartit. Trecem notarea spectrului pe biti.
Daca bitul cel mai semnificativ nu variaza, atunci ele pot fi impartite.
Daca variaza, atunci nu.**

---
2
---

Este dat spectrul de IP-uri
    192.168.0.33 - 192.168.0.65

    1. Reprezentam pe biti
           33 - 0010 0001
           65 - 0100 0001
        *Nr de biti neschimbatori este 1. Deci putem forma 2 ^ 1
        subretele.*
    2. In ce retea se afla spectrul? Care este reteaua de baza?
           
        Se afla in reteaua - 192.168.0.0/25

        Nr de subretele - 2.

        * **Prima subretea**
          192.168.0.0/25
          *Urmeaza analiza subretelei ca-n probleme precedente*

        * **A 2-a subretea**
          192.168.0.128/25
          *Same shit*


---
3
---

Este dat spectrul de IP-uri
    192.168.0.33 - 192.168.63

    33 - 0010 0001
    63 - 0011 1111

    NETID = 1111 1111 . 1111 1111 . 1111 1111 . 001* ****

    2 ^ 3 = 8

    Adresa de baza a spectrului - 192.168.0.32 ( /27)

---
4
---

Este dat spectrul de IP-uri
    10.0.17.222 - 10.1.17.222

    Maska /8

    0000 1010 . 0000 0000 . 0001 0001 . 1101 1110

    0000 1010 . 0000 0001 . 0001 0001 . 1101 1110

    2 ^ 7 = 128 (nr de retele in care a fost impartit)

    Subretea din care face parte spectrul
    10.0.0.0 / 15

-----------
21-Oct-2014
-----------

ARP - address resolution protocal.
Este utilizat pentru a rezolva IP -> MAC.
RARP - reverse address resolution protocol
MAC -> IP

*Arhitectura pe 2 nivele*

Client -> Server

Arhitectura bazat pe cereri (de la client) si raspunsuri de la server.

*Arhitectura pe 3+ nivele*
Arhitectura bazata pe mai multe nivele

Client <--> App server <--> DB/Filesystem

App server este un server de interpretare a contextului.

*DOS*
*DDOS*

**Protocoale**
Fiecare protocol este descris de RFC (Request For COmments).

**Arhitectura client-server**
ActiveDirectory:

*Avantaje*
    - Simplu de administrat,
    - Este nevoie de doar 1 admin,
    - Simplu de configurat.
    - Utilizatorul se poate loga si de pe alt calculator
*Dezavantaje*
    - Daca serverul nu este disponibil, toata structura nu va functiona,
    - Este nevoie de un admin obligatoriu,

**Arhitectura p2p (peer-to-peer)**
*Avantaj*
    - Nu este nevoie de server (decentralizat)
    - Nu este nevoie de admin
*Dezavantaje*
    - Dificil de a administra/controla
    - Nu este fiabil


**FTP - FileTransferProtocol**

Client                          Server

+------+
| GUI  |
+------+        PORT - 20
| PI   | <------------------>   | PI  |
+------+        PORT - 21       +-----+
| DTP  | <------------------>   | DTP |
+------+

PI - protocol interpreter (comenzi)
DTP - Data Transfer Protocol

Alta arhitectura

Server1             Client                  Server2

| PI  |     <------> | PI | <-------------> | PI  |
+-----+                                     +-----+
| DTP |     <-----------------------------> | DTP |

*Comenzi FTP*

    - help
    - get [src] [dst]
      transvera fisierele de la src la dest
    - put [src] [dst]
      incarca fisierul de la client la server
    - mget, mput - mai multe fisiere
    - cd
      change directory
    - pwd
      print working directory
    - lcd
    - dir 
      continutul directoriului curent pe server
    - mkdir
    - dele
    - mdele
    - by
    - bye
    - quit


4-Nov-2014
-------------

Bootstrap Protocol (DHCP protocol se bazeaza pe el)
------------------------------------

La conectarea unei statii in retea, serverul DHCP aloca acestei statii o
adresa IP asociind-o cu MAC adresa a statiei.

SNMP (Simple Network Management Protocol)
Ports
    161 - UDP commands
    162 - Trap

Versions of protocol 1, 2.1, 3 (with encryption)
OID - object identifier.

Se foloseste pentru a verifica starea cartelei de retea.
Are citeva request-uri/metode
    set
    get
    walk

Pentru a verifica starea device-ului, se trimite un get la dispozitivul cu
OID-ul respectiv si el raspunde cu 1, atunci tot e ok, daca nu raspunde,
atunci e down. Alte valori pot fi interpretate in dependenta de aplicatie.

MIB files - fisiere de descriere a dispoizitivelor in legatura cu OID-ul
lor.
MIB browser - interpretator la MIB fisiere.


25-Nov-2014
-------------

Routage en Linux
-----------------

iptables, routes vse dela.



09-Dec-2014
---------------

Use SMTP with `telnet`.
command `telnet {hostname} {port}`
    - SMTP
    - POP3

DNS
-----

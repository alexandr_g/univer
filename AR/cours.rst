AR
====

=============
05.Sep.2014
=============

Continutul cursului
-------------------

    0. + Introducere
    1. Bazele retelei TCP/IP
    2. Administrarea retelelor windows - principii
    3. Administrarea retelelor unix
    4. Configurarea proxy
    5. Elemente de securitate
    6. Administrarea firewall
    7. Routarea dinamica - OSPF, IS-15, 13GP
    6. Elemente de management a retelelor prin SNMP.
    8. Testarea si verificarea retelelor TCP/IP.

Componentele unei retele
------------------------
* Ethernet-ul.
* Router
* Switch
* Firewall
* Server

Infrastructura fizica
-----------------------
* Cablu de cupru (UTP, FTP)
* Cablu optic
* Wireless

Echipamentele de comunicare
----------------------------
* Switch
* Router
* Firewall
* Hub-uri
* Dispozitive de iptelefonie/iptv/ipcamere

Componentele de lucru a unei retele
------------------------------------
* Servere
* Statii de lucru fixe si mobile
* IMprimante de retea
* Administratorul de retea



Modelul OSI
------------

7. Application 
6. 
5. 
4.
3. 
2. Data link (802.2, 802.3)
1. Physical layer


=============
12-Sep-2014
=============

Adresarea IPv4 / IPv6
----------------------

Adresele IPv4 scrise sub forma a 4 octeti (X.Y.Z.W). Numarul total al
adreselor disponibile este 2 ^ 32. Solutiile prezentate anterior nu fac sa
amine momentul epuizarii adreselor IP.

IPv6 are 128 bits, astfel sunt disponibile 2 ^ 128 adrese (3.4 * 10 ^ 38
adrese). Scrierea adreselor IPv6 se face pe 8 grupuri a cite 16 bits.
FIecare grup se reprezinta ca 4 cifre hexazecimale. Separarea intre
grupuri se face cu semnul :.
Pentru a semnifica scrierea se aplica 2 reguli la adresele care contin
0-uri in interior:
    1. 0-urile initiale dintr-un grup se pot omite. 00AB -> AB (00A0 -> A0)
    2. 1 sau mai multe grupuri adiacente care contin doar zerouri se pot
       omite complet, inlocuindu-se cu 2 puncte dublu. Aceasta regula se
       poate aplica o singura data intr-o adresa, in caz contrar nu se
       cunoaste cite grupuri au fost prescurtate.

Fie adresa:
    ABCD:0:0:0:0:0:0:1234
    Poate fi scrisa ca ABCD::1234
    
    0:0:0:0:0:0:1:A
    | - > ::1:A
    
    1:0:0:0:1:0:0:1
    | -> 1::1:0:0:1

Adresele unicast sunt asemeni acelor din IPv4 si identifica o singura
interfata a unui host. 
Adresele **multicast** identifica un grup de hosturi.

Orice adresa IPv4 este formata din 2 parti:
    1. NetworkID.
    2. Host-ul.

Exista 5 clase diferite de adrese, putem examina clasa din care parte
adresa IP prin examinarea primelor 4 biti din adresa IP. 

clasa A:
    Adresele incep de la 1 -> 126
Clasa B:
    Adresele incep de la 128 -> 191
Clasa C:
    Adresele incep de la 192 -> 193
Clasa D:
    Adresele incep de la 194 -> 239
Clasa E:
    Adresele incep de la 240 -> 254

Adresele care incep cu 127 sunt folosite pentru adrese interne a unui nod.
Adresa de loopback nu poate fi accesata decit local 
Adresele clasei D sunt rezervate pentru multicasting, iar cele clasei E
sunt rezervate pentru experimente / utilizare in viitor.

Mai multe IP-uri care acelasi NetworkID formeaza o retea.


21-Nov-2014
--------------

Tema: Retele mobile si tehnologia wireless
--------------------------------------------

1. Generalitati
=================

Denumiri ale retelei Wireless
Wi-Fi este o marca inregistrata a companiei Alias.
WLAN - wireless local area network

Diferentele intre o retea radion si retea Wireless sunt multiple si
favorizeaza reteaua wireless.
Spre deosebire de alte sisteme radio

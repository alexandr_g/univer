parent(pamela,robert).
parent(thomas,robert).
parent(pierre,elizabeth).
parent(marie, elizabeth).
parent(robert, anne).
parent(elizabeth,anne).
parent(robert, patricia).
parent(elizabeth, patricia).
parent(anne, julie).
parent(anne, nathalie).
parent(anne, luc).
parent(jimmy, julie).
parent(jimmy, nathalie).
parent(jimmy, luc).
parent(patricia, marc).
parent(patricia, kevin).
parent(martin, marc).
parent(martin, kevin).
parent(julie, jean).
parent(luc, sylvie).
parent(kevin, tom).
parent(sylvain,jean).
parent(rachel, sylvie).
parent(sonia, tom).
%soeur-frere relation
soeur-frere(X,Y):- parent(Z,X), parent(Z,Y), X\==Y.
%tante-oncle relation
tante-oncle(X, Y):- parent(Z,Y), soeur-frere(X,Z), not( parent(X, Y)).
%petit-enfant relation
petit-enfant(X, Y):- parent(Y,Z), parent(Z,X).
%cousin-cousine relation
cousin-cousine(X, Y):- parent(Z,X), parent(W,Y), soeur-frere(Z,W).

%ex 3
femme(pamela).
femme(marie).
femme(elizabeth).
femme(anne).
femme(patricia).
femme(julie).
femme(nathalie).
femme(rachel).
femme(sonia).
femme(sylvie).

homme(thomas).
homme(pierre).
homme(robert).
homme(jimmy).
homme(martin).
homme(luc).
homme(mark).
homme(kevin).
homme(sylvain).
homme(jean).
homme(tom).

%ex 4 : relation femme et homme en utilisant la premiere variante
soeur(X,Y):-soeur-frere(X,Y), femme(X).
frere(X,Y):-soeur-frere(X,Y), homme(X).
tante(X,Y):-tante-oncle(X, Y), femme(X).
oncle(X,Y):-tante-oncle(X, Y), homme(X).
cousin(X,Y):-cousin-cousine(X, Y), homme(X).
cousine(X,Y):-cousin-cousine(X, Y),femme(X).

%ex 4 : relation femme et homme en utilisant la deuxieme variante
% soeur(X,Y):-parent(Z,X),parent(Z,Y), X\==Y, femme(X).
% frere(X,Y):-parent(Z,X),parent(Z,Y), X\==Y, homme(X).
% tante(X,Y):-parent(Z, Y), soeur(X,Z), femme(X).
% oncle(X,Y):-parent(Z, Y), frere(X,Z), homme(X).
% cousin(X,Y):-parent(A,X), parent(B,Y), soeur(A,B);frere(A,B), homme(X).
% cousine(X,Y):-parent(A,X), parent(B,Y), soeur(A,B);frere(A,B),femme(X).

% relation ancetre
ancetre(X,Y) :- parent(X,Y).
ancetre(X,Y) :- parent(X,Z),ancetre(Z,Y).

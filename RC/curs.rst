============
04.09.14
===========
**Bibliografie:**
Andrew Tannenbaum - Computer networks
Nicolae Tomae - Rețele de calculatoare (Cluj-Napoca, 2006)
Ba... și Andronache - (2014) Inernet și intranet
О*** - Компьютерные сети

**Labs**
 - Rețele de calculatoare (practicum de laborator)
===================
Tema: Introducere.
===================
1. Rețele de calculatoare. Noțiuni generale.
2. Evoluția sistemelor de calcul. De la sisteme locale la rețele de calculatoare.

**Rețele de calculatoare. Noțiuni generale.**

**Rețea de calculatoare** se numește ansamblu de stații și rețeaua de transfer date între acestea, destinat deservirii cererilor utilizatorilor în procesări de date.
Stație poate fi:
	* Un calculator
	* Un echipament de stocare date
	* O imprimantă
	* O cameră de luat vederi
	* Un aparat de telefon
	* ș.a.

Schema generică a rețelei:

Rețeaua constă din 2 subrețele.
1. Subrețeaua magistrală. Care constă din noduri de acces și canal de transfer a datelor ce le conectează.
2. Subreteaua de acces, se conectează la subrețea magistrală sau punct la punct, sau prin concentratoare, sau prin conexiune directă la noduri de conexiune.

Subreteaua de acces are tobologia multiarbore.

Destinatia componentelor:
	* Statiile servesc pentru intrare, stocarea, pastrarea, regasirea, procesarea si redarea datelor. Unele statii indeplinesc doar o parte din aceste functii.
	* Canalul de transfer date serveste pentru transferul datelor între două sau mai multe noduri. Ca și nod poate fi o stație, un concentrator sau un nod de comunicație. Canalul care interconectează doar 2 noduri se numește canal punct-la-punct (point-to-point connection), iar acel care interconectează 3 sau mai multe noduri se numește canal multipunct.
	* Concentratorul de date (CD) serveste pentru comansarea a 2 sau mai multe fluxuri de date într-un singur flux și invers (împărțirea unui flux în mai multe).
	* Nodul de comunicație interconectează canalele incidente sau comutează fluxurile de date între canalele incidente.

Evoluția sistemelor de calcul.
===============================

Etapele:
	1. Folosirea izolată a calculatoarelor electronice.
	2. Clustering. Flosirea clusteril-or de calculatoarea.
	3. Folosirea sistemelor de teleprelucrare a datelor.

    
+--------------------------------+-----------+-------+----------+
| Indicatori                     | 1969      | 2014  | 2014/1969|
+--------------------------------+-----------+-------+----------+
| Productivitatea (flops)        | 12Mf      | 34Pf  | ~3 * 10^6|
+--------------------------------+-----------+-------+----------+
| Capacitatea de transfer bps    | 56kbps    |1000Gps| 2 * 10^6 |
+--------------------------------+-----------+-------+----------+
| Numarul de PC-uri, un          | 4         | 1.2Mlr| 2 * 10^8 |
+--------------------------------+-----------+-------+----------+
| Numarul de useri, un           | 100       | 3Mlrd | 3 * 10^7 |
+--------------------------------+-----------+-------+----------+

11-Sep-2014
------------

Tema: Forme si modalitati de reprezentare a informatiei in retele.
-------------------------------------------------------------------

Subiecte
    + Informatie
    + Mesaj
        * discrete
        * continue
    + Discretizarea mesajelor continue.

    * Cantitatea informatiei.
    * Entropia sursei de informatie.
    * Redundanta mesajelor.

    - Semnal.
    - Transformarea mesajelor in semnal. Codificarea si modularea
      acestuia.

--------
1.
---------

Informatie
    se numeste caracteristicele unor obiecte, procese sau fenomene. 
Datele 
    sunt reprezentari concrete ale informatiei in forme posibil de
    procesat nemijlocit la calculator.

Informatia intre statiile unuei retele se transmit in forma de mesaje. 
**Mesaj** se numeste o totalitate de informatii cu inteles finit. Mesajele
pot fi *discrete* sau *continue*. 
*Mesajele continue* pot fi reprezentate prin functii continue de 1 sau mai
multe argumente. De exemplu: temperatura mediului inconjurator intr-un
punct anumit, tensiunea curentului electric. Pentru o functie continua
este caracteristic, ca orice 2 valori cit de alaturate nu vom lua, exista
valori intre aceste 2 puncte.
*Mesajele discrete* sunt cele pot fi reprezentate prin numere. De exemplu:
un numar de obiecte oarecare, cursul valutar etc.

La baza discretizarii mesajelor continue stau 2 teoreme.
**Teorema Fourriere** 
    orice functie continua de timp, de conduita rezonabila, poate fi
    reprezentata fara careva pierderi de informatie prin suma unor
    sinusoide. Aceasta functie va fi caracterizata prin: spectru de
    frecventa si latimea spectrului de frecventa (diferenta intre
    frecventa sinusoidei cu cea mai mica perioada si frecventa sinusoidei
    cu cea mai mare perioada). O functie continua este de conduita
    rezonabila, daca ea nu contine discontinuitati de gradul 1 (sau 2).
**Teorema Nyquist**
    orice functie continua de timp, de conduita rezonabila, poate fi
    reprezentata fara careva pierderi de informatie printr-un sir de
    valori ale functiei, luate la intervale de timp (Dt) ce nu depasesc 1/(2 * DF)
    unde DF este latimea spectrului de frecventa ale functiei continue.
    Sunt cazuri cind valorile din acest sir finit nu pot fi reprezentate,
    de aceea valorile trebuiesc reprezentate folosind valorile apropiate
    de acele admisibile (aproximate, ce cauzeaza pierderi).
    Eroare discretizarii nu depaseste: dx / (2 * sqrt(3)).

    Pentru discretizarea telefoniei digitale:
    S-a ales diapazonul de frecvente 300-3400 Hz.
    DF = 3900 - 300 + 900 = 4000 Hz
    Numarul de biti, determinat experimental, este 8b (256 de valori).
    Dt = 1 / (2 * DF) = 1 / 8000
    n = 1 / dT = 8000
    Viteza de transmisie a datelor este:
    V = n * log(b=2, v=256) = 64 000 bps

Cantitatea informatiei.
------------------------

Cantitatea de informatiei
    cantitatea de informatiei se masoare in cantitatea de incertitudine la
    intelegerea acelui mesaj.
    De exemplu: avem o sursa care genereaza N mesaje, cu aceeasi
    probabilitate P = 1 / N. Atunci incertitudinea va fi I = 1 - 1 / N = N - 1 / N.
    S-a propus ca numarul I sa nu fie folosit direct dar ca: I = log(b=2,
    v=N) = log(b=2, v = 1 / P) = - log(b=2, v=P)
    Fie ca avem N mesaje, fiecare cu probabilitate diferite, atunci
    incertitudinea va fi **Ii = - log(b=2, v=Pi), i = 1..N**.

Entropie informationala
    se numeste cantitatea medie de informatie intr-un mesaj al sursei.
    Daca avem N mesaje, cu probabilitatea Pi, i = 1..N, atunci entropia va
    fi E = sum(Pi * Ii) = - sum(Pi * log(b=2, v=Pi))

*Dezavantajul acestei abordari este acela ca ea caracterizeaza
diversitatea informatiei si nu continutul acesteia. Diversitatea de
raspunsuri a unei baze de date, ce caracterizeaza capacitatea informationala a bazei de date.*

Reduntanta
    se numeste raportul dintre numarul de elemente suplimentare ale unui
    mesaj si numarul total de elemente in mesaj. R = r / (m + r)
    m - este numarul suficient de elemente pentru redarea informatiei
    mesajului.

Semnal si transformarea mesajului in semnal.
---------------------------------------------

Semnal
    se numeste procesul fizic folosit pentru transmisia eficienta de
    informatii la distanta. Esenta semnalului consta nu in natura
    procesului fizic folosit, ci in redarea intocmai a informatiei in
    mesaj (fara pierderi). Insasi procesul fizic folosit, se numeste
    *purtator de semnal*.
    Exemple de *purtatori de semnal*:
        * Curentul electric (continuu / alternativ)
        * Unde electromagnetice
        * LASER

Transformarea mesajelor in semnale:
    1. Transformarea mesajului in semnal primar.
    #. Codificarea semnalului primar.
    #. Codificarea (modularea) semnalului.

| M | -> | S1 | -> | S2 | -> ... -> | Sn | -> | Codificare | -> | Conversia | -> | S |
        Etapa 1                             | etapa 2       |  Etapa 3        


Baza codului
    elemente de cod diferite care servesc la formarea cuvintelor codului.
    De exemplu la ASCII baza codului este 2 (deoarece se folosesc 0 si 1).
    Lungimea cuvintului este numarul de elemente din cuvint.
    *Daca toate cuvintele au aceeasi lungime codul este uniform, in caz
    contrar este neuniform.*
    Exemplu de cod neuniform: Morze, codul Hamming.
Puterea codului
    numarul de cuvinte de cod diferite folosite. Pentru un cod uniform
    puterea este N = b ^ n. b - baza cuvintului, n - lungimea cuvintului.

Modularea semnalelor
    se numeste modificarea valorilor unor parametri ai semnalului astfel
    ca unor grupuri de elemente de cod diferite sa le fie puse in
    corespondenta elementele semnalului diferite.
    modularea se folosete in cazurile cind transmisia re loc prin canale
    analogice.

Fie trebuie sa modulam sirul de biti
    |    0 | 1   | 1     | 0     | 0     | 1     |
----+------+-----+-------+-------+-------+-------+
+U  |______+-------------+_______________+-------|
----+------+-----+-------+-------+-------+-------+
-U  |------+_____________+---------------+_______|
----+------+-----+-------+-------+-------+-------+
MA - modularea in amplitudine: este semnal cind e unitate (sinusoida), nu
este semnal cind este zerou.
MF - frecventa este mai mare in 0, si mai mica la unitate. Tot semnalul
este o sinusoida de frecventa diferita.
MPhi - se schimba faza. La 0 incepe de la 180 si 1 de la 0.
**Metode de modulare**
    * MA - 2 stari = 1 bit
    * MPhi - 2 stari = 1 bit
    * MF - 2 stari = 1 bit
    * MAF      |
    * MAPthi   |    4 stari = log(b=2, v=4) = 2 bit = 1 dibit
    * MFPhi    |
    * MAFPhi 8 stari = log(b=2, v=8) = 3 bit = 1 tribit

Viteza de modulare (B)
    se numeste numarul total de elemente de semnal generate de catre sursa
    intr-o secunda.
    B = 1 / T
    unde T este durata unui element de semnal.
    [B] = bod (baud)

    **Viteza de modulare != viteza de transmisie a datelor.**

    Legatura intre viteza de transmisie a datelor. Sunt direct
    proportionale.
    V = B * I = B * log(b=2, v=N), unde N - numarul de stari a elementelor
    de semnal.
    De obicei viteza de transmisie a datelor este mai mare ca viteza de
    modulare.


Sisteme de transfer date punct-la-punct.
----------------------------------------

**Subiecte**
    1. Notiune de sistem de transfer date. Si std punct-la-punct.
    #. Componentele std punct-la-punct. Functiile lor de baza.

Sistem de transfer date
    se numeste ansamblu de echipamente si produse program destinat
    transferului datelor intre 2 sau mai multe noduri. Un caz particular
    este std punct-la-punct, care interconecteaza doar 2 noduri. Schema
    unui asemenea sistem in caz general este urmatoarea:
                        +--- | UPE | -- | UCS | -+- | UCS | -- | UPE | +
    | ETD| -> | EG | -- |                                            | -- | EG | -- | ETD |
                        +--- | UPE | -- | UCS | -+- | UCS | -- | UPE | +

    Notari folosite
        :ETD: echipament terminal de date.
        :EG: echipament de group.
        :UPE: unitate de protectie la erori.
        :UCS: unitate de conversie a semnalelor.
        :ETCD: echipament de terminatie a circuitului de date. (UPE + UCS)
        :CC: canal de comunicatie. -+-
        :CD: canal discret.
        :CTD: canal de transfer date.
        :TTD: trunchi de transfer date.


02-Oct-2014
----------------

Tema: Multiplexarea canalelor.
------------------------------

Multiplexarea in timp presupune alocarea intregii latimi de banda fiecarui
canal.

--o     LC      o--
--o \_________/ o--
--o             o--

Se poate aplica la transportarea datelor prin fibra optica. 
Avantaje
    * Nu necesita benzi de protectie intre canale
    * Are pierderi foarte mici
Dezavantaje
    * Modemul trebuie sa lucreze/proceseze date cu aceeasi viteza ca si
      viteza canalului.

**Contentrator de canale**
    De la N clienti locali sunt conexiuni la concentrator, concentratorul
    se conecteaza la destinatie prin M canale.

Rețele cu comutarea canalelor.
================================

Comutatorul de canale formeaza conexiunea intre perechi de segmente din
canale incidente la solicitare.

ISDN este unica tehnologie folosita in prezent cu comutatorul de canale.

Avantaj că este posibilă transmiterea datelor cu viteza constantă
(posibilitatea transmiterii traficului izocron).

Rețele cu comutarea de mesaje
===============================

Aceste rețele folosesc în calitate de noduri de comunicație comutatoarele
de mesaje. 
Recepționeaza mesajul, înscrie în memorie și determina canalul de ieșire pentru acest mesaj, îl înregistrează în
firul de așteptare către acest canal și cînd îi vine rîndul îl transmite
mai departe.

Avantajul de bază ale rețelelor cu comutare de **mesaje** constă într-o
încărcare a canalelor de 4-6 ori mai mari. Al doilea avantaj constă în
aceea că nu se pierde timp cu stabilirea conexiunii. Alt avantaj ar fi
posibilitatea unor viteze de transfer date diferite pe diferite canale ale
traseului pachetelor între 2 stații. Încă un avantaj este echilibrarea mai
înaltă a traficului de date pe diferite canale ale rețelei. 

**Neajunsuri**
    * De regulă nu poate fi realizat dialogul. Cauzat de aceea că mesajul
      se transmite complet de la nodul curent la adiacent.
    * Se cere memorie la noduri pentru a buferizarea temporară a mesajelor
      de tranzit.

Rețele cu comutarea de pachete.
=================================

Folosesc în calitate de noduri de comunicație comutatoarele de pachete
(routerul). Routerul operează similar comutatorului de mesaje, dar că în
loc de mesaje sunt pachete. La fiecare pachet se adauga un prefix și sufix
(header și traider/tail), pachetele se transmit prin rețea la fel cum se
transmit mesajele în rețea cu comutarea de mesaje.

Sunt cîteva metode de comutare de pachete
    * **Datagrama** - fiecare pachet se transmite pe traseu propriu
      *Dezavantaj* poate fi că apar problema ordinii pachetelor - ele pot
      ajunge în diferită ordine. Se rezolvă prin atribuirea numarului de
      secvență fiecărui pachet cu posibilitatea restabilirii în viitor a
      ordinii pachetelor.
      *Avantaj* aceasta metodă este mai flexibilă decît acea cu circuite
      virtuale (poate ruta pe alt canal pachetele, dacă apar probleme cu
      unele canale).
      *Avantaj* este mai rapidă.
    * **Cu circuite virtuale** - prevede transmisia tuturor pachetelor unui
      mesaj pe același traseu.
      *Dezavantaj* Este mai puțin flexibilă.
      *Avantaj* este mai fiabilă, mai sigură. Poate mai rapid acționa în
      cazul pierderii pachetelor.

Comutarea de pachete dispune de toate avantajele comutării de mesaje față
de comutarea pe canalelor (posibilitatea diferitor viteze etc.).
DAR la comutarea de mesaje nu este posibilă comicarea în dialog, în
comutarea pe pachete de obicei este posibil acest lucru.

Rețele cu difuzarea pachetelor.
================================

Astfel de rețele nu conțin noduri de comunicație. Toate stațiile se
conecteaă la unicul canal. Este mai simplă ca arhitectură.
*Problemă* Cărei stații de oferit canalul?

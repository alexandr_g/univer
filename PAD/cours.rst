PAD - programarea aplicatiilor distribuite
==========================================

============
05.Sep.2014
============

Contacts
----------
:email:
    dciorba@yahoo.com
    dciorba@gmail.com
    dumitru.ciorba@ati.utm.md
:site:
    http://ciorba.name
:facebook:
    ciorba.dumitru

Introducere
===========

12-Sep-2014
------------

:Sistem distribuit:
    o colectie de procesare autonome care comunica
    printr-o retea si are urmatoare caracteristici:
        * Inexistenta unui ceas fizic comun.
        * Fara memorie partajata.
        * Separarea geografica.
        * Autonomia.

        * Extensibilitate
        * Integritate - masura in care sistemul tolereaza defectele,
          mentinindu-se intr-o stare corecta.
        * Performante

http://top500.org - grupeaza statistici, repartizarea etc.

19-Sep-2014
------------

IEEE 1471 - Defineste ca un sistem concret are o arhitectura in care se
descrie cum se descrie sistemul, componentele, relatiile, evolutia
sistemului. Insa un sitem poate avea mai multe modele - un set de reguli
dupa care se analizeaza sistemul.

Control - set de reguli instructiuni, care schimba starea sistemului.

26-Sep-2014
------------

**Spatiul distribuit Enslow.**


Dupa Jensen avem 2 tipuri de control - 
:Control individual:    schimbarea starii unei resurse, nu implica alte
                        actiuni asupra altori resurse.
:Control colectiv:  schimbarea starii unei resurse implica schimbarea
                    starii unei resurse implica activitati asupra altor
                    resurse.

Factorii/caracteristice in cazul controlului individual:

**Gradul de egalitate** - este masura in care este distribuita autoritatea si
resonsabilitatatea in sistem. Toate controloarele au acelasi drept de
modificare/acces la resurse.
**Gradul de implicare** - daca controlorul realizeaza controlul doar a unei
resurse.
**Numarul de controloare.**

Factorii/caracteristicele in cazul controlului colectiv:

**Numarul de resurse controlate de mai multe controloare**
**Numarul de controlere ce controleaza resursa.**

Elemente arhitecturale.
-------------------------

    * Model de programare.
      Conceptualizarea masinii fizice pe caretrebuie sa ia programatrul in
      considerente la realizare.

    * Control
        - cum este creat paralelismul?
        - ce ordonare exista intre operatii?
        - cum se sincronizeaza diferite thread-uri de control.

    * Operatii
        - care sunt operatiile atomice?
    * Date
        - ce date sunt private/partajate?
        - cum sunt datele partajate, accesate sau comunicate proceselor?
    * Costuri
        - ce modalitate de evaluare a costurilor de procesare exista?

**Modele clasice de programare**
    
    * Spatiu de adrese partajat.
    * Paralelism de date.
    * Transfer de mesaje.


**Nivelurile de sistem**

+------------------------------------------------------------+
|               Applications, services                       |
+------------------------------------------------------------+
|               Middleware                                   |
+---------------+-------------------------+------------------+
|   OS1         |                         |     OS2          |
| Proc., thrd.  |                         | Proc., thrd.     |
+---------------+-------------------------+------------------+
| PC. Network   |                         | PC, netowrk      |
+---------------+-------------------------+------------------+
